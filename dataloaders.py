"""
Class and functions to load the biomes data and serve it in a convenient
format for pytorch.
"""

# See https://pytorch.org/tutorials/beginner/basics/data_tutorial.html

import numpy as np
from torch import Tensor
from torch.utils.data import Dataset, DataLoader


class BiomeDataset(Dataset):
    """Convenience class to manage the biome data."""

    def __init__(self, xs, ys, labels):
        self.xs = Tensor(xs)
        self.ys = ys
        self.labels = labels

    def __len__(self):
        return len(self.ys)  # number of samples

    def __getitem__(self, idx):
        return self.xs[idx], self.ys[idx]


def get_dataloaders(umap_file, labels_file,
                    test_size=0.25, batch_size=64, shuffle=False,
                    singled_label=None):
    """Return torch dataloaders for training and test data."""
    xs = np.loadtxt(umap_file, delimiter=',', skiprows=1)
    y_labels = np.loadtxt(labels_file, delimiter=',', skiprows=1, dtype=str)

    if singled_label:
        y_labels = [label if label == singled_label else 'other'
                    for label in y_labels]

    labels = sorted(set(y_labels))
    assert len(labels) > 1, 'Only one label (you selected an invalid label?).'

    label2y = {labels[i]: i for i in range(len(labels))}
    ys = np.array([label2y[label] for label in y_labels])

    if shuffle:
        indices = np.arange(ys.shape[0])
        np.random.shuffle(indices)
        xs = xs[indices]
        ys = ys[indices]

    n = int(len(ys) * (1 - test_size))  # we take the first n for training
    data_train = BiomeDataset(xs[:n], ys[:n], labels)
    data_test = BiomeDataset(xs[n:], ys[n:], labels)

    return (DataLoader(data_train, batch_size=batch_size),
            DataLoader(data_test, batch_size=batch_size))


# The functions below are for data that we used to read in other formats.

def get_dataloader_by_type(ttype, origin='csv', batch_size=64):
    """Return a torch dataloader with data of the given type."""
    if origin == 'csv':
        xs, ys, labels = read_csv(ttype)
    elif origin == 'tsv':
        xs, ys, labels = read_tsv(f'data/matrix_clean_{ttype}.tsv')
    elif origin == 'npz':
        xs, ys, labels = read_npz(f'data/{ttype}.npz')
    else:
        raise ValueError(f'unknonw data origin: {origin}')

    # If we want to remove the markers that are all 0:
    #xs = xs[:,get_good_markers(xs)]

    data = BiomeDataset(xs, ys, labels)

    return DataLoader(data, batch_size=batch_size)


def read_csv(ttype):
    """Return the xs, ys, labels extracted from the csv file of the given type.

    Example:
      xs, ys, labels = read_csv('train')
    """
    xs = np.loadtxt(f'data/umaps_{ttype}.csv', delimiter=',', skiprows=1)
    y_labels = np.loadtxt(f'data/labels_{ttype}.csv', delimiter=',', skiprows=1,
                          dtype=str)

    labels = list(set(y_labels))

    label2y = {labels[i]: i for i in range(len(labels))}
    ys = [label2y[label] for label in y_labels]

    return xs, ys, labels


def read_tsv(fname):
    """Return the xs, ys, labels extracted from the given tsv file.

    Example:
      xs, ys, labels = read_tsv('matrix_clean_train.tsv')
    """
    xs, ys, labels = [], [], []
    for line in open(fname):
        label, *values = line.split('\t')

        if label not in labels:
            labels.append(label)

        ys.append(labels.index(label))
        xs.append([float(x) for x in values])

    return xs, ys, labels


def read_npz(fname):
    """Return the xs, ys, labels extracted from the given npz file.

    Example:
      xs, ys, labels = read_npz('train.npz')
    """
    f = np.load(fname)

    return f['xs'], f['ys'], f['labels']


def get_good_markers(xs):
    """Return column indices with some non-zero value in that colum."""
    return np.count_nonzero(xs, axis=0).nonzero()[0]
