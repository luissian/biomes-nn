#!/usr/bin/env python3

"""
Make a predictor for Clau's biome data, using a neural network.
"""

# See
# https://pytorch.org/tutorials/beginner/basics/quickstart_tutorial.html

import sys
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt
from torch import nn, optim, cuda, no_grad, save
from dataloaders import get_dataloaders


class ClauNN(nn.Module):
    """Claudia's neural network to predict biomes from data."""

    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()

        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size))
        # NOTE: Many other layers and architectures can be tried.
        # See for example https://pytorch.org/docs/stable/nn.html

    def forward(self, x):
        return self.linear_relu_stack(x)


def main():
    global device

    args = get_args()

    device = args.device if args.device != 'auto' else (
        'cuda' if cuda.is_available() else 'cpu')
    log = print if not args.quiet else (lambda *args: None)
    log('Using computing device:', device)

    log('Reading training and test data...')
    train_dloader, test_dloader = get_dataloaders(
        umap_file=args.samples,
        labels_file=args.labels,
        test_size=args.test_size,
        batch_size=args.batch_size,
        shuffle=args.shuffle,
        singled_label=args.singled_label)

    log('Creating neural network (model)...')
    nvalues = len(train_dloader.dataset[0][0])  # number of values per sample
    nbiomes = len(train_dloader.dataset.labels)  # number of possible biomes
    model = ClauNN(nvalues, args.hidden_size, nbiomes)
    model = model.to(device)  # move model to computing device

    log('Defining how to measure deviations from the correct answer...')
    loss_fn = nn.CrossEntropyLoss()  # loss function

    log('Setting how to use the resulting gradients to optimize the model...')
    optimizer = optim.SGD(model.parameters(), lr=args.learning_rate)

    log(f'Training neural network for {args.epochs} epochs...\n')
    for i in range(args.epochs):  # repeat training for a number of "epochs"
        log(f'Epoch {i+1}')

        train(train_dloader, model, loss_fn, optimizer, args.quiet)

        if not args.quiet:
            ncorrect, ntotal, loss_avg = test(test_dloader, model, loss_fn)
            accuracy = sum(ncorrect) / sum(ntotal)
            log('Accuracy: %.1f %%, avg loss: %g\n' % (100*accuracy, loss_avg))

    ncorrect, ntotal, _ = test(test_dloader, model, loss_fn)
    print('Correct predictions:')
    for nc, nt, label in zip(ncorrect, ntotal, train_dloader.dataset.labels):
        ac = nc / nt if nt > 0 else 0  # accuracty for this label
        print('  %5d / %-5d (%4.3g %%) -- %s' % (nc, nt, 100*ac, label))

    accuracy = sum(ncorrect) / sum(ntotal)
    print('Final accuracy: %.1f %%' % (100*accuracy))

    if args.output:
        print(f'\nSaving model to {args.output} ...')
        save(model, args.output)
        log(help_using_saved_model)


def get_args():
    """Return the arguments passed in the command line."""
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)

    add = parser.add_argument  # shortcut
    add('--samples', default='data/umap_subsampling_1_10_umap_output.csv',
        help='data file with a list of numbers characterizing each sample')
    add('--labels', default='data/umap_subsampling_1_10_umap_labels.csv',
        help='data file with the label corresponding to each sample')
    add('-e', '--epochs', type=int, default=8,
        help='number of epochs (iterations over the full training data)')
    add('-n', '--hidden-size', type=int, default=100,
        help='number of neurons in the hidden layer')
    add('-t', '--test-size', type=float, default=0.25,
        help='fraction of data reserved for testing (not used for training)')
    add('-s', '--shuffle', action='store_true',
        help='shuffle data before separating into training and testing data')
    add('-l', '--learning-rate', type=float, default=1e-3,
        help='learning rate (size of the step following the gradient)')
    add('-b', '--batch-size', type=int, default=64,
        help='batch size (number of samples to feed per model call)')
    add('--singled-label', help='if set, compare that label vs the rest')
    add('-d', '--device', choices=['cpu', 'cuda', 'auto'], default='auto',
        help='device where to do the computations ("cuda" for GPU)')
    add('-o', '--output',
        help='file where to save the final model parameters')
    add('-q', '--quiet', action='store_true',
        help='be less verbose')

    return parser.parse_args()


def train(dataloader, model, loss_fn, optimizer, quiet=False):
    """Train the model (update its parameters).

    The update is done by comparing the model predictions for inputs
    (from the dataloader) to the correct expected outputs (also coming
    from the dataloader). The comparison uses loss_fn, and the updates
    are done according to the optimizer's strategy.
    """
    model.train()  # set model in training mode

    for batch, (X, y) in enumerate(dataloader):  # dataloader for training data
        X, y = X.to(device), y.to(device)  # move to computing device

        optimizer.zero_grad()  # remove the gradients (for parameter update)

        pred = model(X)  # prediction

        loss = loss_fn(pred, y)  # prediction error

        loss.backward()  # compute all gradients with backpropagation

        optimizer.step()  # parameter update (optimize the model)

        if not quiet and batch % 20 == 0:  # just to show partial information
            done = (batch + 1) * len(X)  # number of samples processed
            total = len(dataloader.dataset)  # total number of samples
            print('  [ %4d / %d ]  loss: %.2f' % (done, total, loss))


def test(dataloader, model, loss_fn):
    """Return model's correct, total and loss when applied to the test data."""
    model.eval()  # set model in evaluation mode

    loss = 0.0  # total loss from all the batches

    # Number of correct guesses and totals per label.
    ncorrect = [0] * len(dataloader.dataset.labels)
    ntotal = [0] * len(dataloader.dataset.labels)

    with no_grad():  # we are not training, so no need to compute the gradient
        for X, y in dataloader:  # this dataloader should give only test data
            X, y = X.to(device), y.to(device)  # move to computing device

            pred = model(X)  # model prediction ("guess")

            loss += loss_fn(pred, y)

            for pred_i, y_i in zip(pred.cpu().numpy(), y.cpu().numpy()):
                if pred_i.argmax() == y_i:
                    ncorrect[y_i] += 1
                ntotal[y_i] += 1


    loss_avg = loss / len(dataloader)  # loss divided by number of batches

    return ncorrect, ntotal, loss_avg


help_using_saved_model = """
To use the model (saved in 'model.pt' for example) from python:

  import torch
  from biome_nn import ClauNN

  model = torch.load('model.pt')  # load the file with the saved model

  data = ...  # data like the one from the dataloader
  prediction = model(data)
"""



if __name__ == '__main__':
    try:
        main()
    except AssertionError as e:
        sys.exit(e)
